package image.screenshot.uploader;

import image.screenshot.Screenshot;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONException;
import org.json.JSONObject;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

/**
 * If you are going to use this uploader often please enter your own AUTH client id.
 * 
 * @author Calum Gardiner
 *
 */
public class ImgurUploader implements ScreenshotUploader {
	
	private String imageData;
	
	private static String url = "https://api.imgur.com/3/image";
	private static String AUTH = "Client-ID a361e10d1cd48e2";

	public ImgurUploader(Screenshot screenshot) {
		this.imageData = encodeToB64String(screenshot.getScreenshot());
	}

	@Override
	public String uploadScreenshot() {
		String response = "";
		try {
			HttpResponse<JsonNode> jsonResponse = Unirest.post(url)
					  .header("Authorization", AUTH)
					  .field("image", imageData)
					  .asJson();
			JSONObject data = jsonResponse.getBody().getObject().getJSONObject("data");
			response = data.getString("link");
		} catch (UnirestException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return response;
	}

	private String encodeToB64String(BufferedImage image) {
		String imageString = null;
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		try {
			ImageIO.write(image, "png", bos);
			byte[] base64bytes = Base64.encodeBase64(bos.toByteArray());
			imageString = new String(base64bytes, "UTF8");
			bos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return imageString;
	}
}
