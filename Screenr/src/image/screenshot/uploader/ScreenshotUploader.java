package image.screenshot.uploader;

/**
 * Uploader interface.
 * 
 * @author Calum Gardiner
 * 
 */
public interface ScreenshotUploader {

	/**
	 * Upload the screenshot.
	 * 
	 * @return URL of the uploaded image
	 */
	public String uploadScreenshot();

}
