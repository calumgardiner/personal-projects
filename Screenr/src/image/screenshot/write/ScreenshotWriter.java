package image.screenshot.write;

import image.screenshot.Screenshot;

import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class ScreenshotWriter {
	
	private Screenshot screenshot;
	
	public ScreenshotWriter(Screenshot screenshot) {
		this.screenshot = screenshot;
	}
	
	public void writeScreenshot(String file) {
		try {
			ImageIO.write(screenshot.getScreenshot(), "png", new File(file));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
