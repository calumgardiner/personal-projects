package image.screenshot;

import java.awt.image.BufferedImage;

/**
 * Screenshot interface.
 * 
 * @author Calum Gardiner
 *
 */
public interface Screenshot {
	
	/**
	 * Take a screenshot
	 */
	public void takeScreenshot();
	
	/**
	 * Get the image of the screenshot
	 * 
	 * @return Image of the screenshot
	 */
	public BufferedImage getScreenshot();

}
