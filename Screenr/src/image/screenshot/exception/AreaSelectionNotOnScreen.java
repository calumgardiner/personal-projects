package image.screenshot.exception;

public class AreaSelectionNotOnScreen extends Exception {

	private static final long serialVersionUID = -2243511106123842311L;

	public AreaSelectionNotOnScreen(String message) {
		super(message);
	}
}
