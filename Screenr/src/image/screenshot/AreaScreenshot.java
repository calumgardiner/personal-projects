package image.screenshot;

import image.screenshot.exception.AreaSelectionNotOnScreen;

import java.awt.Rectangle;

/**
 * Implementation of screenshot which takes a screenshot of an area of the
 * screen.
 * 
 * @author Calum Gardiner
 * 
 */
public class AreaScreenshot extends AbstractScreenshot implements Screenshot {

	public AreaScreenshot(Rectangle screenshotArea)
			throws AreaSelectionNotOnScreen {
		super(screenshotArea);
	}

}
