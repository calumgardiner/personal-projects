package image.screenshot;

import image.screenshot.exception.AreaSelectionNotOnScreen;

import java.awt.AWTException;
import java.awt.HeadlessException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;

/**
 * Abstract screenshot, provides functionality for the other screenshot classes.
 * 
 * @author Calum Gardiner
 * 
 */
public abstract class AbstractScreenshot {

	private BufferedImage screenshot;
	private Rectangle screenshotArea;

	protected AbstractScreenshot(Rectangle screenshotArea)
			throws AreaSelectionNotOnScreen {
		this.screenshot = null;
		this.screenshotArea = screenshotArea;
		Rectangle screenArea = new Rectangle(Toolkit.getDefaultToolkit()
				.getScreenSize());
		if (!screenArea.contains(screenshotArea)) {
			throw new AreaSelectionNotOnScreen(
					"The selected screenshot area is not on the screen.");
		}
	}

	public void takeScreenshot() {
		try {
			screenshot = new Robot().createScreenCapture(this.screenshotArea);
		} catch (HeadlessException e) {
			e.printStackTrace();
		} catch (AWTException e) {
			e.printStackTrace();
		}
	}

	public BufferedImage getScreenshot() {
		return this.screenshot;
	}
}
