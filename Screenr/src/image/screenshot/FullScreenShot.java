package image.screenshot;

import image.screenshot.exception.AreaSelectionNotOnScreen;

import java.awt.Rectangle;
import java.awt.Toolkit;

/**
 * Implementation of screenshot which takes a screenshot of the entire screen.
 * 
 * @author Calum Gardiner
 * 
 */
public class FullScreenShot extends AbstractScreenshot implements Screenshot {

	public FullScreenShot() throws AreaSelectionNotOnScreen {
		super(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));
	}

}
