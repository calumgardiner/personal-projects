package example;

import image.screenshot.FullScreenShot;
import image.screenshot.Screenshot;
import image.screenshot.exception.AreaSelectionNotOnScreen;
import image.screenshot.uploader.ImgurUploader;
import image.screenshot.uploader.ScreenshotUploader;
import image.screenshot.write.ScreenshotWriter;

public class Example {

	public static void main(String args[]) {
		// to take a screenshot invoke the screenshot interface
		Screenshot screenshot = null;
		// this can be instantiated as one of the implementations of Screenshot
		// for example FullScreenShot takes a screenshot of the entire screen
		try {
			screenshot = new FullScreenShot();
		} catch (AreaSelectionNotOnScreen e) {
			e.printStackTrace();
		}
		// take the screenshot by invoking the takeScreenshot method
		screenshot.takeScreenshot();
		// to save the screenshot to a file you can use the ScreenshotWriter
		// class
		ScreenshotWriter writer = new ScreenshotWriter(screenshot);
		// call the writeScreenshot method with the path and filename you want
		writer.writeScreenshot("/Users/calum/screenshot.png");
		// you can also upload the screenshot to a image hoster using a
		// ScreenshotUploader
		ScreenshotUploader uploader;
		// this can be instantiated as one of the implementations of
		// ScreenshotUploader such as ImgurUploader
		uploader = new ImgurUploader(screenshot);
		// call the uploadScreenshot method to upload the screenshot, the string
		// which this returns is the url of the uploaded image.
		String url = uploader.uploadScreenshot();
		System.out.println(url);
	}
}
