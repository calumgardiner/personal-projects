package game.board;

public class Board {

	private Cell[][] cells;
	private int x;
	private int y;

	public Board(int x, int y) {
		this.x = x;
		this.y = y;
		this.cells = new Cell[x][y];
		setBoard();
	}
	
	public void setBoard() {
		for (int x = 0; x < this.x; x++) {
			for (int y = 0; y < this.y; y++) {
				cells[x][y] = new Cell();
			}
		}
	}

	public void setAlive(int x, int y) {
		if (x < this.x && y < this.y && x >= 0 && y >= 0) {
			this.cells[x][y].setAlive();
		}
	}

	public void setDead(int x, int y) {
		if (x < this.x && y < this.y && x >= 0 && y >= 0) {
			this.cells[x][y].setDead();
		}
	}
	
	public Cell[][] getCells() {
		return cells;
	}
	
	public void setCells(Cell[][] newBoard) {
		this.cells = newBoard;
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}

}
