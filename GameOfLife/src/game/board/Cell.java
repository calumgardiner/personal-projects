package game.board;

public class Cell {

	private Status status;

	public Cell() {
		this.status = Status.DEAD;
	}
	
	public Cell(Status status) {
		this.status = status;
	}

	public Status getStatus() {
		return this.status;
	}

	public void setAlive() {
		this.status = Status.ALIVE;
	}

	public void setDead() {
		this.status = Status.DEAD;
	}

}
