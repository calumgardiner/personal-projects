package game.seeds;

import game.board.Board;

public class RandomSeed implements Seed {
	
	public RandomSeed() {
		
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "Random Seed";
	}

	@Override
	public void seedBoard(Board board) {
		for (int x = 0; x < board.getX(); x++) {
			for (int y = 0; y < board.getY(); y++) {
				// random seed
				if (getRandomBoolean())
					board.setAlive(x, y);
				else
					board.setDead(x, y);
			}
		}
	}
	
	private boolean getRandomBoolean() {
		return Math.random() < 0.5;
	}


}
