package game.seeds;

import game.board.Board;

public class BlockSeed implements Seed {
	
	public BlockSeed() {
		
	}

	@Override
	public String getName() {
		return "Block Seed";
	}

	@Override
	public void seedBoard(Board board) {
		board.setBoard();
		int x = board.getX();
		int y = board.getY();
		int blockWidth = board.getX()/4;
		int blockHeight = board.getY()/4;
		for (int xx = (x/2)-(blockWidth/2); xx < (x/2)+(blockWidth/2); xx++) {
			for (int yy = (y/2)-(blockHeight/2); yy < (y/2)+(blockHeight/2); yy++) {
				board.setAlive(xx, yy);
			}
		}
	}

}
