package game.seeds;

import game.board.Board;

public class GliderGunSeed implements Seed {

	public GliderGunSeed() {

	}

	@Override
	public String getName() {
		return "Glider Gun Seed";
	}

	@Override
	public void seedBoard(Board board) {
		board.setBoard();
		int xoff = board.getX() / 2;
		int yoff = board.getY() / 2;

		// left square
		board.setAlive(1 + xoff, 5 + yoff);
		board.setAlive(2 + xoff, 5 + yoff);
		board.setAlive(1 + xoff, 6 + yoff);
		board.setAlive(2 + xoff, 6 + yoff);

		// right square
		board.setAlive(35 + xoff, 3 + yoff);
		board.setAlive(36 + xoff, 3 + yoff);
		board.setAlive(35 + xoff, 4 + yoff);
		board.setAlive(36 + xoff, 4 + yoff);

		// left middle pattern
		// left moon
		board.setAlive(14 + xoff, 3 + yoff);
		board.setAlive(13 + xoff, 3 + yoff);
		board.setAlive(12 + xoff, 4 + yoff);
		board.setAlive(11 + xoff, 5 + yoff);
		board.setAlive(11 + xoff, 6 + yoff);
		board.setAlive(11 + xoff, 7 + yoff);
		board.setAlive(12 + xoff, 8 + yoff);
		board.setAlive(13 + xoff, 9 + yoff);
		board.setAlive(14 + xoff, 9 + yoff);
		// dot and arrow
		board.setAlive(15 + xoff, 6 + yoff);
		board.setAlive(16 + xoff, 4 + yoff);
		board.setAlive(17 + xoff, 5 + yoff);
		board.setAlive(18 + xoff, 6 + yoff);
		board.setAlive(17 + xoff, 6 + yoff);
		board.setAlive(17 + xoff, 7 + yoff);
		board.setAlive(16 + xoff, 8 + yoff);

		// right middle pattern
		board.setAlive(21 + xoff, 3 + yoff);
		board.setAlive(21 + xoff, 4 + yoff);
		board.setAlive(21 + xoff, 5 + yoff);
		board.setAlive(22 + xoff, 3 + yoff);
		board.setAlive(22 + xoff, 4 + yoff);
		board.setAlive(22 + xoff, 5 + yoff);
		board.setAlive(23 + xoff, 2 + yoff);
		board.setAlive(25 + xoff, 2 + yoff);
		board.setAlive(25 + xoff, 1 + yoff);
		board.setAlive(23 + xoff, 6 + yoff);
		board.setAlive(25 + xoff, 6 + yoff);
		board.setAlive(25 + xoff, 7 + yoff);
	}

}
