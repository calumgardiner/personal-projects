package game.seeds;

import game.board.Board;

public interface Seed {
	String getName();

	void seedBoard(Board board);
}
