package game.engine;

import game.board.Board;
import game.board.Cell;
import game.board.Status;

import java.util.ArrayList;
import java.util.List;

public class Life {

	private Board board;
	private boolean running = true;

	public Life(Board board) {
		this.board = board;
	}
	
	public void setRunning() {
		running = true;
	}
	
	public void stopRunning() {
		running = false;
	}
	
	public boolean isRunning() {
		return running;
	}

	public void tick() {
		if (running) {
			// get the current cells
			Cell[][] cells = board.getCells();
			// make a copy so nothing is changed until next iteration
			Cell[][] cellcopy = new Cell[board.getX()][board.getY()];
			for (int x = 0; x < board.getX(); x++) {
				for (int y = 0; y < board.getY(); y++) {
					cellcopy[x][y] = new Cell(cells[x][y].getStatus());
				}
			}
			for (int x = 0; x < board.getX(); x++) {
				for (int y = 0; y < board.getY(); y++) {
					// update the board
					List<Cell> neighbours = getNeighbours(x, y, cellcopy);
					int aliveneighbours = 0;
					for (Cell cell : neighbours) {
						if (cell.getStatus() == Status.ALIVE) {
							aliveneighbours++;
						}
					}
					if (aliveneighbours < 2)
						board.setDead(x, y);
					if (aliveneighbours == 2
							&& cells[x][y].getStatus() == Status.ALIVE)
						board.setAlive(x, y);
					if (aliveneighbours == 3)
						board.setAlive(x, y);
					if (aliveneighbours > 3)
						board.setDead(x, y);
				}
			}
		}
	}

	private List<Cell> getNeighbours(int x, int y, Cell[][] cells) {
		int width = board.getX();
		int height = board.getY();
		List<Cell> neighbours = new ArrayList<Cell>();
		int xminus1 = (x == 0) ? width - 1 : x - 1;
		int yminus1 = (y == 0) ? height - 1 : y - 1;
		int xplus1 = (x == (width - 1)) ? 0 : x + 1;
		int yplus1 = (y == (height - 1)) ? 0 : y + 1;
		// position 1
		neighbours.add(cells[xminus1][yminus1]);
		// position 2
		neighbours.add(cells[x][yminus1]);
		// position 3
		neighbours.add(cells[xplus1][yminus1]);
		// position 4
		neighbours.add(cells[xminus1][y]);
		// position 5
		neighbours.add(cells[xplus1][y]);
		// position 6
		neighbours.add(cells[xminus1][yplus1]);
		// position 7
		neighbours.add(cells[x][yplus1]);

		// position 8
		neighbours.add(cells[xplus1][yplus1]);
		return neighbours;
	}

}
