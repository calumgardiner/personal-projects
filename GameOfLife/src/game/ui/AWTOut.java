package game.ui;

import game.board.Board;
import game.engine.Life;
import game.seeds.BlockSeed;
import game.seeds.GliderGunSeed;
import game.seeds.RandomSeed;
import game.seeds.Seed;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

public class AWTOut extends JFrame {

	private static final long serialVersionUID = 2701015641262503863L;

	public static void main(String[] args) {
		final AWTOut gui = new AWTOut();
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
			}
		});
		gui.startSimulation();
	}

	private UIBoard uiBoard;
	private Board board;
	private Life life;
	private Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	private int gridsize = 2;
	private int refresh = 50;

	// add your custom implemented seed here
	private Seed[] seeds = { new RandomSeed(), new GliderGunSeed(), new BlockSeed() };

	public AWTOut() {
		setSystemLook();
		setTitle("Conway's Game of Life");
		setExtendedState(JFrame.MAXIMIZED_BOTH);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		Container contentPane = getContentPane();
		contentPane.setLayout(new BorderLayout());
		setupLifeBoard();
		uiBoard = new UIBoard(gridsize, board);
		add(uiBoard, BorderLayout.CENTER);
		setupButtons();
		setVisible(true);
		repaint();
	}

	private void setupLifeBoard() {
		// set up the board
		int width = (screenSize.width) / gridsize;
		int height = (screenSize.height - 200) / gridsize;
		board = new Board(width, height);
		// set up the life
		life = new Life(board);
	}

	public void startSimulation() {
		while (true) {
			life.tick();
			uiBoard.repaint();
			try {
				Thread.sleep(refresh);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	private void setupButtons() {
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.PAGE_AXIS));
		JPanel controlPanel = new JPanel(new FlowLayout());
		JButton pause = new JButton("Start/Stop");
		pause.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (life.isRunning())
					life.stopRunning();
				else
					life.setRunning();
			}
		});
		controlPanel.add(pause);
		JButton clear = new JButton("Clear");
		clear.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				board.setBoard();
			}
		});
		controlPanel.add(clear);
		JLabel speed = new JLabel("Speed: ");
		controlPanel.add(speed);
		JButton slower = new JButton("<<");
		slower.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (refresh >= 200)
					refresh = 200;
				else
					refresh += 10;
			}
		});
		controlPanel.add(slower);
		JButton faster = new JButton(">>");
		faster.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (refresh == 0)
					refresh = 0;
				else
					refresh -= 10;
			}
		});
		controlPanel.add(faster);
		JPanel seedPanel = new JPanel(new FlowLayout());
		JLabel label = new JLabel("Seed: ");
		seedPanel.add(label);
		for (final Seed seed : seeds) {
			JButton button = new JButton(seed.getName());
			button.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					life.stopRunning();
					seed.seedBoard(board);
				}
			});
			seedPanel.add(button);
		}
		buttonPanel.add(controlPanel);
		buttonPanel.add(seedPanel);
		add(buttonPanel, BorderLayout.PAGE_END);
	}

	private void setSystemLook() {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
