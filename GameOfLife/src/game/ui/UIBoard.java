package game.ui;

import game.board.Board;
import game.board.Cell;
import game.board.Status;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

public class UIBoard extends JPanel {

	private static final long serialVersionUID = -2546770532801924760L;

	private int x;
	private int y;
	private int width;
	private int height;
	private Board board;
	private int squaresize;

	public UIBoard(int squaresize, Board board) {
		this.x = board.getX();
		this.y = board.getY();
		this.squaresize = squaresize;
		this.width = x * squaresize;
		this.height = y * squaresize;
		this.board = board;
		this.setSize(width, height);
	}
	
	public void setSquareSize(int size) {
		this.squaresize = size;
	}

	@Override
	public void paintComponent(Graphics g) {
		// paint to buffer
		Graphics g2 = g.create();
		// white background
		g2.setColor(Color.white);
		g2.fillRect(0, 0, width, height);
		// draw x axis grid
		g2.setColor(Color.gray);
		for (int x = 0; x <= this.x; x++)
			g2.drawLine((x * squaresize), 0, (x * squaresize), height);
		// draw y axis grid
		for (int y = 0; y <= this.y; y++)
			g2.drawLine(0, (y * squaresize), width, (y * squaresize));
		Cell[][] cells = board.getCells();
		g2.setColor(Color.black);
		for (int x = 0; x < this.x; x++) {
			for (int y = 0; y < this.y; y++) {
				if (cells[x][y].getStatus() == Status.ALIVE) {
					// fill alive cell
					g2.fillRect(x*squaresize, y*squaresize, squaresize, squaresize);
				}
			}
		}
		// copy buffer to graphics
		g = g2;
	}
}
