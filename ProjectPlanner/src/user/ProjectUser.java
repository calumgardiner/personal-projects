package user;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Implementation of a User for a project.
 * 
 * @author Calum Gardiner
 * 
 */
public class ProjectUser implements User {

	private String userName;
	private String password;

	public ProjectUser() {
		this.userName = "";
		this.password = "";
	}

	public ProjectUser(String userName, String password) {
		this.userName = userName;
		this.password = md5Hash(password);
	}

	@Override
	public String getUserName() {
		return this.userName;
	}

	@Override
	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Override
	public void setUserPassword(String password) {
		this.password = md5Hash(password);
	}

	@Override
	public boolean checkPassword(String password) {
		if (this.password == md5Hash(password))
			return true;
		return false;
	}

	/**
	 * Generate an MD5 hash of the given string.
	 * 
	 * @param message
	 * 
	 * @return MD5 Hash of message
	 */
	private static String md5Hash(String message) {
		try {
			byte[] hashBytes = MessageDigest.getInstance("MD5").digest(
					message.getBytes("UTF-8"));
			BigInteger hashInteger = new BigInteger(1, hashBytes);
			String hash = hashInteger.toString(16);
			return (hash.length() < 32 ? "0" + hash : hash);
		} catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return null;
	}

}
