package user;

/**
 * Interface of the java representation of a User.
 * 
 * @author Calum Gardiner
 * 
 */
public interface User {

	/**
	 * Get the user name.
	 * 
	 * @return User Name
	 */
	public String getUserName();

	/**
	 * Set the user name
	 * 
	 * @param userName
	 */
	public void setUserName(String userName);

	/**
	 * Set the user password.
	 * 
	 * @param password
	 */
	public void setUserPassword(String password);

	/**
	 * Check the user password.
	 * 
	 * @param password
	 * 
	 * @return true if correct
	 */
	public boolean checkPassword(String password);

}
