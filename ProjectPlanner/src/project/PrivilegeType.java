package project;

public enum PrivilegeType {
	
	FULLACCESS {
		public String toString() {
			return "Full Access";
		}
	},
	READONLY {
		public String toString() {
			return "Read Only";
		}
	},
	NOACCESS {
		public String toString() {
			return "No Access";
		}
	}
}
