package project.exception;

public class EntityNotFoundException extends Exception {

	private static final long serialVersionUID = -655284883609415348L;

	public EntityNotFoundException(String message) {
		super(message);
	}
	
}
