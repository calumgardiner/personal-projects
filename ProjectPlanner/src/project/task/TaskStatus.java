package project.task;

/**
 * Enum representing the status of a task.
 * 
 * @author Calum Gardiner
 * 
 */
public enum TaskStatus {

	INCOMPLETE {
		public String toString() {
			return "Incomplete";
		}
	},

	COMPLETE {
		public String toString() {
			return "Complete";
		}
	},

	CANCELLED {
		public String toString() {
			return "Cancelled";
		}
	}
}
