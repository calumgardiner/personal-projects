package project.task;

import java.util.Date;

/**
 * Basic Implementation of a task.
 * 
 * @author Calum Gardiner
 * 
 */
public class BasicTask implements Task {

	public String taskName;
	public TaskStatus taskStatus;
	public Date startDate;
	public Date deadline;

	public BasicTask(String taskName, TaskStatus taskStatus, Date startDate, Date deadline) {
		this.taskName = taskName;
		this.taskStatus = taskStatus;
		this.startDate = startDate;
		this.deadline = deadline;
	}

	public BasicTask() {
		this.taskName = "";
		this.taskStatus = TaskStatus.INCOMPLETE;
		this.startDate = null;
		this.deadline = null;
	}

	@Override
	public String getTaskName() {
		return this.taskName;
	}

	@Override
	public void setTaskName(String name) {
		this.taskName = name;
	}

	@Override
	public TaskStatus getTaskStatus() {
		return this.taskStatus;
	}

	@Override
	public void setTaskStatus(TaskStatus status) {
		this.taskStatus = status;
	}

	@Override
	public void setStartDate(Date date) {
		this.startDate = date;
	}

	@Override
	public Date getStartDate() {
		return this.startDate;
	}

	@Override
	public void setDeadlineDate(Date date) {
		this.deadline = date;
	}

	@Override
	public Date getDeadlineDate() {
		return this.deadline;
	}

}
