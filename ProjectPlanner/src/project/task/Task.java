package project.task;

import java.util.Date;

/**
 * Interface of the java representation of a task within a project.
 * 
 * @author Calum Gardiner
 * 
 */
public interface Task {

	/**
	 * Get the task name.
	 * 
	 * @return Task Name
	 */
	public String getTaskName();

	/**
	 * Set the task name.
	 * 
	 * @param name
	 */
	public void setTaskName(String name);

	/**
	 * Get the task status.
	 * 
	 * @return Task Status
	 */
	public TaskStatus getTaskStatus();

	/**
	 * Set the task status.
	 * 
	 * @param status
	 */
	public void setTaskStatus(TaskStatus status);
	
	/**
	 * Set the task start date.
	 * 
	 * @param date
	 */
	public void setStartDate(Date date);
	
	/**
	 * Get the task start date.
	 * 
	 * @return Start Date
	 */
	public Date getStartDate();
	
	/**
	 * Set the task deadline date.
	 * 
	 * @param date
	 */
	public void setDeadlineDate(Date date);

	/**
	 * Get the task deadline date.
	 * 
	 * @return Deadline Date
	 */
	public Date getDeadlineDate();
	
}
