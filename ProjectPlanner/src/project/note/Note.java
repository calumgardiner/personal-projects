package project.note;

/**
 * Interface of the java representation of a note within a project.
 * 
 * @author Calum Gardiner
 * 
 */
public interface Note {
	
	/**
	 * Set the note title.
	 * 
	 * @param title
	 */
	public void setNoteTitle(String title);
	
	/**
	 * Get the note title.
	 * 
	 * @return title
	 */
	public String getNoteTitle();
	
	/**
	 * Set the note text.
	 * 
	 * @param text
	 */
	public void setNoteText(String text);
	
	/**
	 * Get the note text.
	 * 
	 * @return note text
	 */
	public String getNoteText();

}
