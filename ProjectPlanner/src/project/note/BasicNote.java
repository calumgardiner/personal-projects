package project.note;

/**
 * Basic Implementation of a note.
 * 
 * @author Calum Gardiner
 * 
 */
public class BasicNote implements Note {
	
	private String noteTitle;
	private String noteText;
	
	public BasicNote() {
		this.noteTitle = "";
		this.noteText = "";
	}
	
	public BasicNote(String title, String text) {
		this.noteTitle = title;
		this.noteText = text;
	}

	@Override
	public void setNoteTitle(String title) {
		this.noteTitle = title;
	}

	@Override
	public String getNoteTitle() {
		return this.noteTitle;
	}

	@Override
	public void setNoteText(String text) {
		this.noteText = text;
	}

	@Override
	public String getNoteText() {
		return this.noteText;
	}

	
}
