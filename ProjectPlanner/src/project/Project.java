package project;

import java.util.List;
import java.util.Map;

import project.note.Note;
import project.task.Task;
import user.User;

/**
 * Interface of the java representation of a project.
 * 
 * @author Calum Gardiner
 * 
 */
public interface Project {

	/**
	 * Return the project name.
	 * 
	 * @return Project Name
	 */
	public String getProjectName();

	/**
	 * Set the project name.
	 * 
	 * @param name
	 */
	public void setProjectName(String name);

	/**
	 * Return the project info.
	 * 
	 * @return Project Info
	 */
	public String getProjectInfo();

	/**
	 * Set project info.
	 * 
	 * @param info
	 */
	public void setProjectInfo(String info);

	/**
	 * Return the tasks in the project.
	 * 
	 * @return list of the project tasks
	 */
	public List<Task> getTasks();

	/**
	 * Add a task to the project.
	 * 
	 * @param task
	 */
	public void addTask(Task task);

	/**
	 * Remove a task from the project.
	 * 
	 * @param task
	 */
	public void removeTask(Task task) throws Exception;

	/**
	 * Get the users of this project.
	 * 
	 * @return List of users
	 */
	public List<User> getUsers();

	/**
	 * Add a user to this project.
	 * 
	 * @param user
	 */
	public void addUser(User user);

	/**
	 * Remove a user from the project.
	 * 
	 * @param user
	 */
	public void removeUser(User user) throws Exception;
	
	/**
	 * Return the privileges of all users.
	 * 
	 * @return Map of users and privileges
	 */
	public Map<User, PrivilegeType> getPrivileges();
	
	/**
	 * Get the privilege of a user.
	 * 
	 * @param user
	 * 
	 * @return Privilege type of the user
	 */
	public PrivilegeType getPrivilege(User user) throws Exception;
	
	/**
	 * Set the privilege of a user.
	 * 
	 * @param user
	 * 
	 * @param privilege
	 */
	public void setPrivilege(User user, PrivilegeType privilege);
	
	/**
	 * Get the notes for the project.
	 * 
	 * @return list of notes
	 */
	public List<Note> getNotes();
	
	/**
	 * Add a note to the project.
	 * 
	 * @param note
	 */
	public void addNote(Note note);
	
	/**
	 * Remove a note from the project.
	 * 
	 * @param note
	 */
	public void removeNote(Note note) throws Exception;
	
}
