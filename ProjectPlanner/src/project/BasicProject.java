package project;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import project.exception.EntityNotFoundException;
import project.note.Note;
import project.task.Task;
import user.User;

/**
 * Generic Implementation of a task.
 * 
 * @author Calum Gardiner
 *
 */
public class BasicProject implements Project {

	private String projectName;
	private String projectInfo;
	private List<Task> tasks;
	private List<User> users;
	private Map<User, PrivilegeType> privileges;
	private List<Note> notes;

	public BasicProject(String projectName, String projectInfo) {
		this.projectName = projectName;
		this.projectInfo = projectInfo;
		this.tasks = new ArrayList<Task>();
		this.users = new ArrayList<User>();
		this.privileges = new HashMap<User, PrivilegeType>();
		this.notes = new ArrayList<Note>();
	}
	
	public BasicProject() {
		this.projectName = "";
		this.projectInfo = "";
		this.tasks = new ArrayList<Task>();
		this.users = new ArrayList<User>();
		this.privileges = new HashMap<User, PrivilegeType>();
		this.notes = new ArrayList<Note>();
	}
	
	@Override
	public String getProjectName() {
		return projectName;
	}

	@Override
	public void setProjectName(String name) {
		this.projectName = name;
	}

	@Override
	public String getProjectInfo() {
		return projectInfo;
	}

	@Override
	public void setProjectInfo(String info) {
		this.projectInfo = info;
	}

	@Override
	public List<Task> getTasks() {
		return this.tasks;
	}

	@Override
	public void addTask(Task task) {
		this.tasks.add(task);
	}

	@Override
	public void removeTask(Task task) throws EntityNotFoundException {
		if (this.tasks.contains(task))
			this.tasks.remove(task);
		else
			throw new EntityNotFoundException("Task not found in project.");
	}

	@Override
	public List<User> getUsers() {
		return this.users;
	}

	@Override
	public void addUser(User user) {
		this.users.add(user);
	}

	@Override
	public void removeUser(User user) throws EntityNotFoundException {
		if (this.users.contains(user))
			this.users.remove(user);
		else 
			throw new EntityNotFoundException("User not found in project.");
	}

	@Override
	public Map<User, PrivilegeType> getPrivileges() {
		return this.privileges;
	}

	@Override
	public PrivilegeType getPrivilege(User user) throws EntityNotFoundException {
		if (this.privileges.containsKey(user))
			return this.privileges.get(user);
		else
			throw new EntityNotFoundException("User not found in project.");
	}

	@Override
	public void setPrivilege(User user, PrivilegeType privilege) {
		this.privileges.put(user, privilege);
	}

	@Override
	public List<Note> getNotes() {
		return this.notes;
	}

	@Override
	public void addNote(Note note) {
		this.notes.add(note);
	}

	@Override
	public void removeNote(Note note) throws Exception {
		if (this.notes.contains(note))
			this.notes.remove(note);
		else 
			throw new EntityNotFoundException("The note was not found in the project.");
	}

}
